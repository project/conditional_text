<div class="condition-wrapper">
  <form action="">
    <fieldset class="collapsible<?php print $result ? '' : ' collapsed' ?>">
      <legend>
        <span class='fieldset-legend'>
          <?php print $reason; ?>
        </span>
      </legend>
      <div class='fieldset-wrapper'>
        <span class='text'><?php print $text; ?></span>
      </div>
    </fieldset>
  </form>
</div>
