<?php

/**
 * @file
 * Tests for the Conditional Text API.
 *
 * @see conditional_text_api
 */

/**
 * Test class for the API.
 */
class ConditionalTextAPITest extends DrupalWebTestCase {

  public static function getInfo() {
    return array(
      'name' => 'Conditional Text API test',
      'description' => '',
      'group' => 'Conditional Text',
    );
  }

  public function setUp() {
    parent::setUp(array('conditional_text'));
  }

  /**
   * Helper function to check if the conditional_text_tokenize()
   * function produces correct output to a given
   * sample.
   *
   * @param string $sample
   *   String to check.
   * @param array $expected
   *   List of tokens.
   */
  protected function checkTokenize($sample, $expected) {
    module_load_include('inc', 'conditional_text');
    $result = conditional_text_tokenize($sample);
    $this->assertFalse(array_diff($result, $expected),
      t('Tokenizer produces the correct output on: "%sample"',
      array('%sample' => $sample)));
  }

  /**
   * Tests conditional_text_tokenize().
   */
  public function testTokenize() {
    $testdata = array(
      'a b>=c d<=e = f' => array(
        'a', 'b', '>=', 'c', 'd', '<=', 'e', '=', 'f',
      ),
      'a==b and b == c' => array(
        'a', '==', 'b', 'and', 'b', '==', 'c',
      ),
    );
    foreach ($testdata as $sample => $expected) {
      $this->checkTokenize($sample, $expected);
    }
  }

  /**
   * Helper function to check if conditional_text_evaluate_conditions()
   * properly works.
   *
   * @param string $expression
   * @param bool $result
   */
  protected function checkEvaluator($expression, $result) {
    module_load_include('inc', 'conditional_text');
    $conditions = conditional_text_tokenize($expression);
    $this->assertEqual($result,
      conditional_text_evaluate_conditions($conditions, array(),
                      'conditional_text_evaluate_bool_condition'),
      t('Expression "%expression" is successfully evaluated to %result',
        array('%expression' => $expression,
              '%result' => $result ? t('true') : t('false'))));
  }

  /**
   * Tests conditional_text_evaluate_conditions().
   */
  public function testEvaluator() {
    $testdata = array(
      'true' => TRUE,
      'false' => FALSE,
      'not true' => FALSE,
      'not false' => TRUE,
      'true and false' => FALSE,
      'false or true' => TRUE,
      'true and not false' => TRUE,
      'not false xor true' => FALSE,
      'true and true and false' => FALSE,
      'true and true and not false' => TRUE,
    );
    foreach ($testdata as $expression => $result) {
      $this->checkEvaluator($expression, $result);
    }
  }

  /**
   * Helper function to check the evaluator.
   *
   * @param string $text
   *   Condition arguments.
   * @param bool $result
   *   Expected result.
   * @param null|array $context
   *   Context array without the display key.
   */
  protected function checkRealEvaluator($text, $result, $context = NULL) {
    $this->assertEqual(conditional_text_evaluate($text,
       array('display' => 'filter') + ($context ?: array())),
     $result,
     t('Expression "%expression" is successfully evaluated to %result',
       array('%expression' => $text,
         '%result' => $result ? t('true') : t('false'))));
  }

  /**
   * Tests for conditional_text_evaluate().
   */
  public function testRealEvaluation() {
    $user = $this->drupalCreateUser();
    $user->roles = array(
      'authenticated user',
      'administrator',
    );
    $this->drupalLogin($user);

    $context = array(
      'display' => 'filter',
      'custom' => array(
        'types' => array(
          array(
            'identifier_token' => 'Custom',
            'identifier_token_mn' => 'custom',
            'options' => array(
              array(
                'option_name' => array(
                  'name' => 'Foo',
                  'name_mn' => 'foo',
                ),
                'values' => array(
                  'anonymous user' => 1,
                  'authenticated user' => 0,
                  'administrator' => 0,
                ),
              ),
              array(
                'option_name' => array(
                  'name' => 'Bar',
                  'name_mn' => 'bar',
                ),
                'values' => array(
                  'anonymous user' => 0,
                  'authenticated user' => 1,
                  'administrator' => 0,
                ),
              ),
              array(
                'option_name' => array(
                  'name' => 'Baz',
                  'name_mn' => 'baz',
                ),
                'values' => array(
                  'anonymous user' => 0,
                  'authenticated user' => 0,
                  'administrator' => 1,
                ),
              ),
            ),
          ),
          array(
            'identifier_token' => 'Experience',
            'identifier_token_mn' => 'experience',
            'options' => array(
              array(
                'option_name' => array(
                  'name' => 'Beginner',
                  'name_mn' => 'beginner',
                ),
                'values' => array(
                  'anonymous user' => 1,
                  'authenticated user' => 1,
                  'administrator' => 0,
                ),
              ),
              array(
                'option_name' => array(
                  'name' => 'Advanced',
                  'name_mn' => 'advanced',
                ),
                'values' => array(
                  'anonymous user' => 0,
                  'authenticated user' => 1,
                  'administrator' => 0,
                ),
              ),
              array(
                'option_name' => array(
                  'name' => 'Master',
                  'name_mn' => 'master',
                ),
                'values' => array(
                  'anonymous user' => 0,
                  'authenticated user' => 0,
                  'administrator' => 1,
                ),
              ),
            ),
          ),
        ),
      ),
      'enabled' => array(
        'modules' => array(
          'data' => array(
            array('module' => 'foo-7.x-1.0'),
            array('module' => 'bar-6.x-5.0'),
            array('module' => 'baz-7.x-1.x'),
            array('module' => 'aaa-7.x-1.0-alpha1'),
            array('module' => 'bbb-7.x-1.0-beta2'),
            array('module' => 'ccc-7.x-1.0-rc3'),
          ),
        ),
      ),
    );

    $testdata = array(
      /*
       * Condition plugin
       */
      // no version syntax
      'enabled not_enabled_module' => FALSE,
      'enabled conditional_text' => TRUE,
      'enabled foo' => TRUE,
      'enabled bar' => TRUE,
      // short syntax
      'enabled user >= 7.0' => TRUE,
      'enabled node < 5.0' => FALSE,
      'enabled foo >= 1.0' => TRUE,
      'enabled bar >= 2.0' => TRUE,
      // full syntax
      'enabled menu >= 7.x-7.0' => TRUE,
      'enabled system < 6.x-1.0' => FALSE,
      'enabled foo == 7.x-1.0' => TRUE,
      // core related
      'enabled drupal = ' . VERSION => TRUE,
      'enabled drupal = 6.x' => FALSE,
      'enabled drupal = 7.x' => TRUE,
      // advanced versions
      'enabled foo = 7.x-1.x' => TRUE,
      'enabled bar = 6.x-5.x' => TRUE,
      'enabled baz = 7.x-1.0' => TRUE,
      'enabled aaa < 7.x-1.0' => TRUE,
      'enabled bbb > 7.x-1.0-alpha5' => TRUE,
      'enabled bbb < 7.x-1.0-rc1' => TRUE,
      'enabled ccc < 7.x-1.0' => TRUE,
      /*
       * Boolean plugins (true and false)
       */
      // simple cases
      'true' => TRUE,
      'false' => FALSE,
      // boole expressions
      'true and true' => TRUE,
      'not false xor false' => TRUE,
      'not not true' => TRUE,
      /**
       * Custom plugin
       */
      'custom custom foo' => FALSE,
      'custom custom bar' => TRUE,
      'custom custom baz' => TRUE,
      'custom experience beginner' => TRUE,
      'custom experience advanced' => TRUE,
      'custom experience master' => TRUE,
      /*
       * Mixed expressions
       */
      'enabled menu and true' => TRUE,
      'not false xor enabled system < 5.x-1.0' => TRUE,
    );

    foreach ($testdata as $expression => $result) {
      $this->checkRealEvaluator($expression, $result, $context);
    }
  }

}
